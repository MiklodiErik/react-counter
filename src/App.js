import * as ReactBootstrap from "react-bootstrap";

export default function App() {
  return <div className="App"></div>;
}

/* Only for presentation */
function Test() {
  function handleClick() {
    console.log("clicked");
  }

  function handleMouseOver() {
    console.log("mouse over");
  }

  return (
    <>
      <ReactBootstrap.Button variant="danger" onClick={handleClick}>
        click on me
      </ReactBootstrap.Button>
      <ReactBootstrap.Button variant="warning" onMouseOver={handleMouseOver}>
        mouse over button
      </ReactBootstrap.Button>
    </>
  );
}
